from django.conf.urls import url
from .views import index, add_todo, del_todo
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_todo', add_todo, name='add_todo'),
    url(r'^del_todo', del_todo, name='del_todo'),
    # url(r'^del_todo/(?P<todo_id>\d+)/$', del_todo, name='del_todo'),
    # url(r'^(?P<slug>[\w-]+)/delete/$', del_todo),
]

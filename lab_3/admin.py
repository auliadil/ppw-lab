from django.contrib import admin
from .models import *

# Register your models here.
class DiaryAdmin(admin.ModelAdmin):
	pass
admin.site.register(Diary, DiaryAdmin)
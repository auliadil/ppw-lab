/* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
jQuery(function($){
	$( '.menu-btn' ).click(function(){
		$('.responsive-menu').addClass('expand')
		$('.menu-btn').addClass('btn-none')
	})
	
	 $( '.close-btn' ).click(function(){
		$('.responsive-menu').removeClass('expand')
		$('.menu-btn').removeClass('btn-none')
	})
})	
from django.db import models
from django.utils import timezone
import pytz

# Create your models here.
class Message(models.Model):
    name = models.CharField(max_length=27)
    email = models.EmailField()
    message = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

    # def __str__(self):
    #     return self.message

    # def convertTZ():
    # 	return timezone.now() + timezone.timedelta(hours =7)

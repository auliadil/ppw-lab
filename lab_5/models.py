from django.db import models

class Todo(models.Model):
    title = models.CharField(max_length=27)
    description = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    todo_id = models.IntegerField(default=0)


# Create your models here.
